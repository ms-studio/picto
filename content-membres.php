<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		
		// Get Posted date
		
		$updated = get_the_date( 'Y-d-m' ); 
		
		// Post thumbnail.
		// twentyfifteen_post_thumbnail();
		
		// echo picto_slideshow();
		
		$picto_slideshow = picto_slideshow();
		
		if ( !empty( $picto_slideshow ) ) {
		
			echo '<div class="member-header-gallery">';	
			echo $picto_slideshow;
			echo '</div>';
		
		}
		
	?>

	<header class="entry-header">
		<h1 class="entry-title updated published" datetime="<?php echo $updated; ?>">
		<?php
			echo picto_nom_prenom( get_the_title() );
		?>
		</h1>
		<?php 
		
		$acf_champ_activite = get_field( "acf_champ_activite" );
		$acf_num_atelier = get_field( "acf_num_atelier" );
		
		$picto_member_details = false;
		
		if ( $acf_champ_activite != '' ) {
			$picto_member_details = true;
		} else if ( $acf_num_atelier != '' ) {
			$picto_member_details = true;
		}
		
		if ( $picto_member_details == true ) {
					
					echo '<div class="member-details">';
					
					if ( $acf_champ_activite != '' ) {
						
						echo '<span class="member-activity">'.$acf_champ_activite.'</span>';
						
					}
					
					if ( $acf_num_atelier != '' ) {
						
						echo '<span class="member-atelier bold">atelier '.$acf_num_atelier.'</span>';
						
					}
					
					echo '</div>';
		
		}
		
		 ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php

			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );
			
			
			/* Links
			 ***********************/

			// links 
			
			$external_links =  get_post_meta($post->ID, 'acf_lien_externe_bloc', false);
			
			$otherlinks = array();
			
			if ( !empty($external_links) ) {
				  
				 while ( has_sub_field ( 'acf_lien_externe_bloc' ) ) : 
				 	
				 			$otherlinks[] = get_sub_field('acf_lien_externe');
				 			
				 endwhile; 
			}
			 
			// courriel
			
			$acf_courriel = get_field( "acf_courriel" ); 
			 
			if ( $otherlinks || $acf_courriel ) {
			
				echo '<div class="meta-fields vcard">';
				echo '<span class="fn">'.picto_nom_prenom( get_the_title() ).'</span>';
						
						if ($otherlinks) {
							  
							   echo picto_hyperlinks( $otherlinks );
							  
						} 
				    
				    if ($acf_courriel) {
				    	
				    	$acf_courriel = vldr_process_hyperlinks($acf_courriel);
				    	
				    		echo '<div class="acf-courriel">';
				      	echo '<p>' . $acf_courriel. '</p>';
				      	echo '</div>';
				    }
				    
				  echo '</div>';  
				    
				}
				
		?>
	</div><!-- .entry-content -->


</article><!-- #post-## -->
