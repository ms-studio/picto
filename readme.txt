=== PICTO ===
Contributors: Manuel Schmalstieg
Requires at least: WordPress 4.1
Tested up to: WordPress 4.6-trunk
Version: 0.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: black, blue, gray, pink, purple, white, yellow, dark, light, two-columns, left-sidebar, fixed-layout, responsive-layout, accessibility-ready, custom-background, custom-colors, custom-header, custom-menu, editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments, translation-ready

== Description ==
A custom WordPress theme based on Twenty Fifteen.

For more information about Twenty Fifteen please go to https://codex.wordpress.org/Twenty_Fifteen.

