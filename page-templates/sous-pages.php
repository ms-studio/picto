<?php
/**
 * Template Name: Sous-Pages
 *
 * Description: Grille de sous-pages
 *
 * Note: modèle utilisé pour Hors-Cases, Résidences
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main sous-pages" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			// get_template_part( 'content', 'page' );
			
			?>
			<div id="grid" class="grid hentry clear" data-columns>
			<?php

			// Make a query for child pages.
						
						$current_page_id = get_the_ID();
						
						$list_query = new WP_Query( array(
								 	'posts_per_page' => -1,
								 	'post_type' => 'page',
								 	'post_parent' => $current_page_id,
								 	'orderby' => 'menu_order',
			  					'order' => 'ASC',
								 	) ); 
								 	
								 	if ($list_query->have_posts()) : 
					  			 			  			 	         
					  			 while( $list_query->have_posts() ) : $list_query->the_post();
					  			 
					  			 	$news_array[] = picto_create_news() ;
					  			 
					  			 endwhile; 
					  			 
								endif;
						wp_reset_postdata();
			
						// Generate Output
						if ($news_array) {
			
							foreach ($news_array as $key => $item) {
																		
									echo picto_echo_news( $item, 'sub-page' );
									
							}
							
						}
						
		?>
		</div><!-- #grid -->
		<?php
		
		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
		
		<aside class="site-main site-aside">
			<?php get_template_part( 'content', 'memberlist' ); ?>
		</aside>
		
	</div><!-- .content-area -->

<?php get_footer(); ?>
