<?php
/**
 * Template Name: Membres
 *
 * Description: Liste des membres
 *
 * @package WordPress
 * @subpackage Pictonet
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<div id="members" class="members">
			<nav class="hentry member-list">
		<?php
		
		$members_list = picto_members_output();
		
		echo $members_list;
		
		?>
			</nav>
		</div><!-- #members -->

		</main><!-- .site-main -->
		
		<aside class="site-main site-aside">
			<?php get_template_part( 'content', 'memberlist' ); ?>
		</aside>
		
	</div><!-- .content-area -->

<?php get_footer(); ?>