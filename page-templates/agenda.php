<?php
/**
 * Template Name: Agenda
 *
 * Description: List of events
 *
 * @package WordPress
 * @subpackage Pictonet
 */

get_header(); 

	// Main Page Loop
	while ( have_posts() ) : the_post();
	
				// Generate gallery with ACF images.
				$img_info = gallery_init('medium');
				
				$picto_content = apply_filters( 'the_content', get_the_content() );
				
				$picto_content = vldr_process_hyperlinks( $picto_content );
					
	// End the loop.
	endwhile;

?>

<script>
	
	var ImgCount = 0;
	
	<?php 
	
		if (!empty($img_info)) {
			
			echo "var PictoHasGallery = true;\r";
			
			$img_count = count($img_info);
			
			echo "var ImgCount = ".$img_count.";\r";
			
			echo "var PictoItems = new Array(\r";
			
				foreach ($img_info as $key => $item){
								
					echo '"'.$item["url-large"] . '"';
					
					if ( ( $key + 1 ) < $img_count ) {
						echo ",\r";
					}
								
				} // foreach
				
			echo ");\r";	
			
		} else { // !empty

			echo "var PictoHasGallery = false;";
			
		} 
		
	 ?>
	
	// Create Modal Image Box
	var box = document.createElement('div');
	
	box.className = "box pictophotos";
	
	box.innerHTML = '<div class="pictobackground"></div><h1 onclick="PictoModalExit();return false;"><a href="#" class="condensed">PICTO</a></h1>';
	
	// Prevent scrolling when modal is open
	document.querySelector("html").style.overflow = 'hidden';
	document.querySelector("body").style.overflow = 'hidden';
	document.querySelector("html").style.position = 'fixed';
	document.querySelector("body").style.position = 'fixed';

	
	if ( ImgCount > 1 ) {
	
		// Several images? = add Previous and Next Buttons
		box.innerHTML += ' <a class="prev" href="#" onclick="PictoModalPrev();return false;">&lt;</a>';
		
		box.innerHTML += ' <a class="next" href="#" onclick="PictoModalNext();return false;">&gt;</a>';
	} 
	
	<?php 
	
		// Affichage des informations sur la galerie (nom artiste, titre série).
	
		if ( !empty( $picto_content ) ) {
			
			// Suppression des retours de ligne.
			$picto_content = preg_replace( '/(^|[^\n\r])[\r\n](?![\n\r])/', '$1 ', $picto_content );
			
			// Affichage
			echo "box.innerHTML += ' <div class=\"legende condensed\">".$picto_content."</div>'\r";
		}
	
	 ?>
	
	var page = document.getElementById("page");
	
	function getRandom(min, max) {
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	
		<?php
		
		if ( !isset($_COOKIE["pictomodalexit"]) ) {
	
			// Test if we have a gallery
			
			?>
			
			if ( PictoHasGallery == false ) {
				 	
				 	
				 } else {
				 
				 // Randomization: 
				 // Start at a random position in the loop.
				 
				 if ( ImgCount > 1 ) {
				 	
				 	var PictoStartPosition = getRandom( 1, ImgCount ) - 1;
				 	console.log("position = "+PictoStartPosition);
				 	
				 }
				 
				 // Set first image
				 	var PictoBg = "url("+PictoItems[PictoStartPosition]+")";
				 	var PictoBgNr = PictoStartPosition;
				 	
				 	// Get window height
				 	var jsWinHeight = window.innerHeight;
				 	
				 	box.style.height = jsWinHeight+'px';
				 	
				 	// Create box
				 	document.querySelector("body").insertBefore(box, page);
				 	
				 	var boxbackground = box.firstChild;
				 	
//				 	PictoImgReplace();
				 	var PictoBg = "url("+PictoItems[PictoBgNr]+")";
				 	boxbackground.style.opacity = 0;
				 	
				 	(function($) {
						$(".pictobackground").fadeOut(0, function() {
						    
					    boxbackground.style.backgroundImage = PictoBg;
					    boxbackground.style.backgroundSize = "cover";
					    
					    $(".pictobackground").fadeIn(1200);
					    boxbackground.style.opacity = 1;
						});
				 	    
				 	})(jQuery);
				 	
				 }
			
			<?php

		} else { 

			// Cookie is set - show nothing!  
			echo "box.innerHTML = '';\r";

		}
		
		?>
	
	function PictoModalPrev() {
		// replace background image, getting previous image from array
		if ( PictoBgNr == 0 ) {
			// we are on first image - go to last item of array = count - 1.
			PictoBgNr = (ImgCount - 1);
		} else {
			PictoBgNr--;
		}
		PictoImgReplace();
	}
	
	function PictoModalNext() {
		// replace background image, getting next image from array
		if ( PictoBgNr == (ImgCount - 1) ) {
			// we are on last image - go to first item of array
			PictoBgNr = 0;
		} else {
			PictoBgNr++;
		}
		
		PictoImgReplace();
	}
	
	function PictoImgReplace() {
		var PictoBg = "url("+PictoItems[PictoBgNr]+")";
		
		(function($) {

        $(".pictobackground").fadeOut(1200, function() {
            
            boxbackground.style.backgroundImage = PictoBg;
            boxbackground.style.backgroundSize = "cover";
            
            $(".pictobackground").fadeIn(1200);
        });
        
        // make Logo smaller
        
        $(".pictophotos").addClass("photos-navigating");
        
    })(jQuery);
		
	}
	
	function PictoModalExit() {
		
		box.style.height = "0px";
		box.style.opacity = "0";
		
		var date = new Date();
		date.setTime(date.getTime() + (30 * 1000)); // 30 seconds
		expires = "; expires=" + date.toGMTString();
		
		// $("body").removeClass("modal-open")
		
		document.querySelector("html").style.overflow = 'auto';
		document.querySelector("body").style.overflow = 'auto';
		document.querySelector("html").style.position = 'static';
		document.querySelector("body").style.position = 'static';
		 
//		document.cookie = "pictomodalexit=visited" + expires + "; path=/"; 
//		console.log("cookie set!");
		
	}
	
// Intercept events : 
// Left Key
// Right Key

document.onkeydown = function(evt) {
    evt = evt || window.event;
    switch (evt.keyCode) {
        case 37:
            // leftArrowPressed();
            PictoModalPrev();
            break;
        case 39:
            // rightArrowPressed();
            PictoModalNext();
            break;
    }
};

// Scrolldown	

var ticking = false;

window.addEventListener('mousewheel', function(e) {
  
    if (!ticking) {
    window.requestAnimationFrame(function() {
          
      PictoModalExit();

      ticking = false;
    
    });
    
  }
  ticking = true;

});

</script>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<div id="grid" class="grid hentry clear" data-columns>
		<?php
		
		// Query for Events
		
		// Current year: 
		
		$news_array_current = picto_archive_query( date("Y") );
		
		$news_array_other_year = '';
		
		// are we in december? 
		
		if ( date("n") == 12 ) {
			
			// Query for the next year:
			
			$news_array_other_year = picto_archive_query( ( date("Y") ) + 1 ); 
			
		}
				
		// Are we in June or earlier?
		
		if ( date("n") <= 6 ) {
			
			// Query for the previous year:
			
			$news_array_other_year = picto_archive_query( ( date("Y") ) - 1 ); 
			
		} 

		// Note: array_merge doesn't do anything when one array is empty (unset)
		// See: http://stackoverflow.com/questions/20131948/php-array-merge-if-not-empty
		// http://stackoverflow.com/questions/19711491/merge-array-returns-null-if-one-or-more-of-arrays-is-empty
		
		$news_array = array();
		
		if ( is_array( $news_array_current ) ) {
		    $news_array = array_merge($news_array, $news_array_current);
		}
		
		if ( is_array( $news_array_other_year ) ) {
		    $news_array = array_merge($news_array, $news_array_other_year);
		}
		
		
		// Filter 1 : remove events that are more than 30 days away!
		
		// define limit:
		$future_limit = ( 30 * DAY_IN_SECONDS );
		
		$mem_today = mem_date_of_today();
		
		// define limit in unix format
		$future_limit_unix = ( $mem_today["unix"] + $future_limit );
		
		$news_array = array_filter( $news_array, 
				function($i) use ($future_limit_unix) { 
						return $i['start-date-unix'] <= $future_limit_unix; 
		});
		
		
		// Filter 2 : Limit the number of items!
		
		$maxnumber = 50 ;
		
		if ( count($news_array) > $maxnumber ) {
		
			$news_array = array_slice( $news_array, 0, $maxnumber );
			
			// array_slice() returns the sequence of elements from the array array 
			// as specified by the offset and length parameters.
			
		}
		

  	/*************** 
  	 *** OUTPUT ***
  	*************** */
  	
  	if (!empty($news_array)) {
  			
  		global $mem_today_short;
  		
  		$archive_type = 'default';		
  				
  		foreach ($news_array as $key => $item) {
  				
  			// display items!
  			echo picto_echo_news( $item, 'agenda' );
  				
  		}
  			
  	}
		
		?>
		</div><!-- #grid -->

		</main><!-- .site-main -->
		
		<aside class="site-main site-aside">
			<?php get_template_part( 'content', 'memberlist' ); ?>
		</aside>
		
	</div><!-- .content-area -->

<?php get_footer(); ?>