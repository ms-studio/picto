<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();
						
			$current_post_id = get_the_ID();
			
			$current_year = picto_get_year( $current_post_id );
			
			if ( is_singular( 'membres' ) ) {
			
				get_template_part( 'content', 'membres' );
			
			} else {
			
				get_template_part( 'content', get_post_format() );
			
			}


			// Previous/next post navigation.
			
			if ( is_singular( 'membres' ) ) {
			
				// Build links:
				$prevnext = picto_prevnext_byname( $current_post_id );
				
				// Output:
				echo picto_prevnext_output( $prevnext, 'membres' );
						
			} else if ( is_singular( 'post' ) ) {
				
				// Build links
				$prevnext = picto_prevnext_bydate( 
					$current_post_id,
					$current_year 
					 );
				
				// Output:
				echo picto_prevnext_output( $prevnext, 'post' );
			
			} else {
			
				$picto_post_navigation = get_the_post_navigation( array(
				
					'next_text' => '<span class="meta-nav" aria-hidden="true">&gt;</span> ' .
						'<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
						'<span class="post-title">%title</span>', // = 'prev_text'
					'prev_text' => '<span class="meta-nav" aria-hidden="true">&lt;</span> ' .
						'<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
						'<span class="post-title">%title</span>', // 'next_text'
					true, // in_same_term - Whether link should be in a same taxonomy term
					'', // excluded_terms - Array or comma-separated list of excluded term IDs.
					'category', // - Taxonomy, if $in_same_term is true
					
				) );
			
				echo $picto_post_navigation;
			
			}

		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
		
		<aside class="site-main site-aside">
			<?php get_template_part( 'content', 'memberlist' ); ?>
		</aside>
		
	</div><!-- .content-area -->

<?php get_footer(); ?>
