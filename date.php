<?php
/**
 * The template for displaying date archive pages
 
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->
			
			<div id="grid" class="grid hentry clear" data-columns>
			
			<?php
			
			$exclude_id = array();
			
			$current_year = get_the_time('Y');
			
			if ( have_posts() ) : 
			// Start the Loop.
			// Note: date query is tweaked via Pre_Get!!
			while ( have_posts() ) : the_post();
			
			endwhile; // End the loop.
			else :
			endif;
				
			
			if ( is_year() ) {
			
				// picto_archive_query = performs several queries based on current year.
			
				$news_array = picto_archive_query( $current_year );
			
			}
					
			// Generate Output
			if ($news_array) {

				foreach ($news_array as $key => $item) {
						
						echo picto_echo_news( $item, 'archive' );
						
				}
				
			}
			
			?>
			</div><!-- #grid -->

		</main><!-- .site-main -->
		
		<aside class="site-main site-aside">
			<?php get_template_part( 'content', 'memberlist' ); ?>
		</aside>
		
	</section><!-- .content-area -->

<?php get_footer(); ?>
