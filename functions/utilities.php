<?php 



/* Turn links into hyperlinks
******************************/

function vldr_process_hyperlinks($vldr_content) {
			
			$vldr_content = ' ' . $vldr_content;
			$attribs = ''; 
			$vldr_content = preg_replace(
				array(
					// '#([\s>])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is',
					// '#([\s>])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is',
					'#([\s>])([a-z0-9\-_.]+)@([^,< \n\r]+)#i'
					),
				array(
					// '$1<a href="$2"' . $attribs . '>$2</a>',
					// '$1<a href="http://$2"' . $attribs . '>$2</a>',
					'$1<a class="email" href="mailto:$2@$3">$2@$3</a>'),$vldr_content);
			$vldr_content = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $vldr_content);
			$vldr_content = trim($vldr_content);
			
			if ( function_exists('eae_encode_emails') ) {
					$vldr_content = eae_encode_emails($vldr_content);
			}
			
			return $vldr_content;
}



/* Disable hyperlinks
******************************/

function picto_disable_hyperlinks($content) {
			
			$content = strip_tags( 
				$content, 
				'<br><div><p><b><strong><i><em><span>' // allowable_tags - removes <a> tag
			);
			
			$content = trim($content);
			
			if ( function_exists('eae_encode_emails') ) {
					$content = eae_encode_emails($content);
			}
			
			return $content;
}


/**
 * Searches for plain email addresses in given $string and
 * encodes them (by default) with the help of eae_encode_str().
 * 
 * Regular expression is based on based on John Gruber's Markdown.
 * http://daringfireball.net/projects/markdown/
 * 
 * @param string $string Text with email addresses to encode
 * @return string $string Given text with encoded email addresses
 */
  
function eae_encode_emails($string) {

	// abort if $string doesn't contain a @-sign
	if (apply_filters('eae_at_sign_check', true)) {
		if (strpos($string, '@') === false) return $string;
	}

	// override encoding function with the 'eae_method' filter
	$method = apply_filters('eae_method', 'eae_encode_str');

	// override regex pattern with the 'eae_regexp' filter
	$regexp = apply_filters(
		'eae_regexp',
		'{
			(?:mailto:)?
			(?:
				[-!#$%&*+/=?^_`.{|}~\w\x80-\xFF]+
			|
				".*?"
			)
			\@
			(?:
				[-a-z0-9\x80-\xFF]+(\.[-a-z0-9\x80-\xFF]+)*\.[a-z]+
			|
				\[[\d.a-fA-F:]+\]
			)
		}xi'
	);

	return preg_replace_callback(
		$regexp,
		create_function(
            '$matches',
            'return '.$method.'($matches[0]);'
        ),
		$string
	);

}

/**
 * Encodes each character of the given string as either a decimal
 * or hexadecimal entity, in the hopes of foiling most email address
 * harvesting bots.
 *
 * Based on Michel Fortin's PHP Markdown:
 *   http://michelf.com/projects/php-markdown/
 * Which is based on John Gruber's original Markdown:
 *   http://daringfireball.net/projects/markdown/
 * Whose code is based on a filter by Matthew Wickline, posted to
 * the BBEdit-Talk with some optimizations by Milian Wolff.
 *
 * @param string $string Text with email addresses to encode
 * @return string $string Given text with encoded email addresses
 */
function eae_encode_str($string) {

	$chars = str_split($string);
	$seed = mt_rand(0, (int) abs(crc32($string) / strlen($string)));

	foreach ($chars as $key => $char) {

		$ord = ord($char);

		if ($ord < 128) { // ignore non-ascii chars

			$r = ($seed * (1 + $key)) % 100; // pseudo "random function"

			if ($r > 60 && $char != '@') ; // plain character (not encoded), if not @-sign
			else if ($r < 45) $chars[$key] = '&#x'.dechex($ord).';'; // hexadecimal
			else $chars[$key] = '&#'.$ord.';'; // decimal (ascii)

		}

	}

	return implode('', $chars);

}

/**
 * Function: picto_nom_prenom()
 *
 * Processing "Last name, First name" pairs
 *
 * @param string $nom_prenom : The original post title ("Wilde, Oscar").
 *
 * @return string : The human-readable title ("Oscar Wilde").
 * 
 * Usage: 
 * $post_title = firstname_lastname(get_the_title());
 *
 * URL: https://gist.github.com/ms-studio/8793349
 *
 */

function picto_nom_prenom($post_title) {
	
	$pos = strpos($post_title, ",");
      		
	if ($pos === false) {
	  // no comma - nothing to do.
  } else {
      $fir = explode( ",", $post_title ); 
      unset( $fir[0] ); 
      $end = ltrim( implode( ",", $fir ) ); 
      
      $first_name = substr($post_title, 0, $pos);
      $pos++;
      
      $post_title = substr($post_title, $pos);
      
      // add space ... UNLESS last char is an "’"
      
      $title_last_char = mb_substr( $post_title, -1);
      $title_last_seven = mb_substr( $post_title, -7);
      
      if ( ( $title_last_char == "’" ) || ( $title_last_seven == "&rsquo;" ) ) {
      	// do nothing
      } else {
      	// add space
      	$post_title .= ' ';
      }
      
      // add first name
      $post_title .= $first_name;
	 }
	
	return $post_title;
}

/*
 * Apply it to the title
 * Info: 
 * https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
 * https://make.wordpress.org/core/2015/10/20/document-title-in-4-4/
*/

add_filter('document_title_parts', 'picto_override_post_title', 10);

function picto_override_post_title($title){

    if( is_singular( 'membres' ) ){ 
        // change title parts here
        
        $nom_entier = get_the_title();
        
        if (function_exists('picto_nom_prenom')) {
            $nom_entier = picto_nom_prenom($nom_entier);
        }
        
        $title['title'] = $nom_entier; 
        $title['site'] = get_bloginfo( 'name', 'display' );
    }
    
    return $title; 
}

/*
 * Prevent Page Scroll When Clicking the More Link
 * https://codex.wordpress.org/Customizing_the_Read_More#Link_Jumps_to_More_or_Top_of_Page
*/

function remove_more_link_scroll( $link ) {
	$link = preg_replace( '|#more-[0-9]+|', '', $link );
	return $link;
}
add_filter( 'the_content_more_link', 'remove_more_link_scroll' );


/*
 * Favicon URLs to header
*/
add_action('wp_head', 'picto_header_favicons', 9);

// priority 1: before RSS feed
// priority 7: before CSS
// priority 8: before JavaScript
// priority 9: before API, RSD etc

function picto_header_favicons()
{
		echo '<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />';
		echo "\r\n";
		echo '<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />';
		echo "\r\n";
		echo '<link rel="apple-touch-icon" href="/apple-touch-icon.png" />';
		echo "\r\n";
}
