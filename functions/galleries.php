<?php 




// ********* Produce Image Gallery **********


function picto_slideshow() {
		
		// Generate gallery with ACF images.
		 $img_info = gallery_init('medium');
		 

		 // Si une seule image, simplement retourner la balise image
		 // Si plusieurs, générer la galerie.
		 		 
		 if ( !empty($img_info) ) {

				if ( count($img_info) == 1 ) {
				
				// echo '<p>single image</p>';
				
					foreach ($img_info as $key => $item){

							 return '<img src="'.$item["url-medium"].'" style="width: '.$item["width-medium"].'px; height: '.$item["height-medium"].'px; left: 0px; top: 0px;" class="single-header-img">';
							
					}
						
				
				} else {
		 
			 		$img_id_array = array();
			 		
					 foreach ($img_info as $key => $item){
					 	
					 		// get id
					 		$img_id_array[] = $item["id"];
					 		
					 		// get medium size
					 		$img_width_array[] = $item["width-medium"];
					 	}
					
					$img_id_list = implode(",", $img_id_array);
									
					return do_shortcode( '[gallery ids="'.$img_id_list.'" loop="true" keyboard="true" link=file width='.$img_width_array[0].']' );
					
					// more info: https://wordpress.org/plugins/fotorama/installation/ 
					
					}

				
		} // !empty
		
}


/**
 * 1) Image Gallery Init.
 *
 * INPUT:
 * @param string $size : a registered image size.
 *
 * OUTPUT:
 * @return array $img_info : A functional image gallery array (with more custom size URLs than the ACF Gallery Array).
 * 
 */

function gallery_init($size = 'thumbnail') {

// 1) Test for acf_galerie_images
//    $has_gallery = false;
    $img_info = array();
    
    $galeries_photos = get_field('acf_galerie_images');
    
//     var_dump( get_field('acf_galerie_images') );
    		
    if ($galeries_photos) {
    
    		// tester aussi si $galeries_photos[0] > zero
    		// car le champ peut-être là, mais vide.

					if  ( $galeries_photos[0] > 0) {
					
						$has_gallery = true;
						$img_info = gallery_toolbox($galeries_photos,$size);
						
						// gallery_toolbox() is located in gallery-function.php
					
					}			
    				
    }
    
   return $img_info;
    
} // end function gallery_init()


/**
 * 2) Image Gallery Array Generator.
 *
 *
 * @param array $img_list : The array produced by ACF Gallery Field.
 * @param string $size : a registered image size (thumbnail, medium, large, full...)
 * 
 * @return array $img_gallery_array : A functional image gallery array (with more custom size URLs than the ACF Gallery Array). 
 */

function gallery_toolbox($img_list = array(),$size = 'thumbnail') {
	
	$img_gallery_array = array();
	
	// idea: test for featured image
	// and put it first
		
	foreach ( $img_list as $image ) {
					 
					 // var_dump($image);
					 
					 // test for image mime types
					 // allowed: image/jpeg
					 // not allowed: image/tiff
					 
					 $img_mime_types = array("image/jpeg", "image/png", "image/gif");
					 
					 if (in_array($image["mime_type"], $img_mime_types)) {
					
							$img_gallery_array[] = array( 
									"id" => $image["id"],
									"url-custom" => $image["sizes"][$size],
									"width-custom" => $image["sizes"][$size."-width"],
									"height-custom" => $image["sizes"][$size."-height"],
									
									"url-medium" => $image["sizes"]["medium"],
									"width-medium" => $image["sizes"]["medium-width"],
									"height-medium" => $image["sizes"]["medium-height"],
									
									"url-large" => $image["sizes"]["large"],
									"width-large" => $image["sizes"]["large-width"],
									"height-large" => $image["sizes"]["large-height"],
									
									"caption" => $image["caption"],
									"alt" => $image["alt"],
									"title" => $image["title"],
									// "gallery-title" => $gallery_title,
									// "gallery-descr" => $gallery_description,
							);
							
						} // else: wrong mime type
					
			} // end foreach
	 							
	 return $img_gallery_array;
	 
} // end of function gallery_toolbox()



/* improvement to gallery shortcode */
//function fix_my_gallery_wpse43558($output, $attr) {
//
//}
//add_filter("post_gallery", "fix_my_gallery_wpse43558", 10, 2);


