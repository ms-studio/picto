<?php 

/*
 * picto_archive_query
 *********************
 *
 * Permet de construire une archive par date, qui prend aussi bien en compte
 * les dates de publication que les dates d'événement.
 *
 * Cette fonction est utilisée sur:
 * - la page Agenda (cf page-templates/agenda.php)
 * - les pages archive par année (cf date.php)
 * - la navigation Previous/Next
 *
 * Le résultat peut être généré avec picto_echo_news()
 *
*/


function picto_archive_query( $current_year ) {
		
		if ( is_user_logged_in() ) {
		    
		    delete_transient( 'picto_agenda_'.$current_year );
		    
		}
		
		if ( false === ( $news_array = get_transient( 'picto_agenda_'.$current_year ) ) ) {
		
				$exclude_id = array();
				
				$no_date_query = new WP_Query( array( 
							'posts_per_page' => -1,
							'post__not_in' => $exclude_id,
							'year' => $current_year,
							
							'meta_query' => array(
								    array(
								     'key' => '_mem_start_date',
								     'compare' => 'NOT EXISTS'
								    ),
							),
				));
							
				if ( $no_date_query->have_posts() ) :
		  			  while( $no_date_query->have_posts() ) : $no_date_query->the_post();
		  			 		
		  			 		$exclude_id[] = get_the_ID();
		  			 		$news_array[] = picto_create_news() ;
		
		  				endwhile; 
				endif;
							
				// Second query: look for posts WITH Event Date = current year
					
				$has_date_query = new WP_Query( array( 
								'posts_per_page' => -1,
								'post__not_in' => $exclude_id,
								'meta_query' => array(
								    'relation' => 'AND',
								    array(
								        'key' => '_mem_start_date',
								        'value' => $current_year,
								        'compare' => '>=',
								    ),
								    array(
								        'key' => '_mem_start_date',
								        'value' => $current_year.'-12-32',
								        'compare' => '<=',
								    ),
									)
					));
					
					if ( $has_date_query->have_posts() ) :
							  while( $has_date_query->have_posts() ) : $has_date_query->the_post();
									  		  					 		  					 
									 $exclude_id[] = get_the_ID();
									 
									 $news_array[] = picto_create_news() ;
									 		  		 				    			 				
						 endwhile; 
					endif; 
									     	     
					// END of second query	
							
					// SORT everything (by "start-date-iso")
					if ( !empty( $news_array ) ) {
							usort($news_array, "picto_news_array_sort");
					}	
					
					set_transient( 'picto_agenda_'.$current_year , $news_array, 6 * HOUR_IN_SECONDS  ); 
					// * HOUR_IN_SECONDS
			
			} // end of get_transient test
			
			// return the result
			
			return $news_array;

}


/*
 * picto_cat_query
 *********************
 *
 * Permet de construire une archive par catégorie (en pied de page).
 * La catégorie est sélectionnée via un champ ACF.
 *
 * Le résultat sera généré avec picto_echo_news()
 *
*/

function picto_cat_query( $category_id ) {
		
		if ( is_user_logged_in() ) {
		    
		    delete_transient( 'picto_cat_'.$category_id );
		    
		}
		
		if ( false === ( $news_array = get_transient( 'picto_cat_'.$category_id ) ) ) {
		
				$exclude_id = array();
				
				$no_date_query = new WP_Query( array( 
							'posts_per_page' => -1,
							'post__not_in' => $exclude_id,
							'cat' => $category_id,
				));
							
				if ( $no_date_query->have_posts() ) :
		  			  while( $no_date_query->have_posts() ) : $no_date_query->the_post();
		  			 		
		  			 		$exclude_id[] = get_the_ID();
		  			 		$news_array[] = picto_create_news() ;
		
		  				endwhile; 
				endif;
							
					// SORT everything (by "start-date-iso")
					if ( !empty( $news_array ) ) {
							usort($news_array, "picto_news_array_sort");
					}	
					
					set_transient( 'picto_cat_'.$category_id , $news_array, 6 * HOUR_IN_SECONDS  ); 
					// * HOUR_IN_SECONDS
			
			} // end of get_transient test
			
			// return the result
			
			return $news_array;

}

/**
 * Helper function to sort items by "start-date-iso"
 * 
 */

function picto_news_array_sort($a,$b) {
     return $a['start-date-iso']<$b['start-date-iso'];
}



/**
 * CREATE NEWS:
 * Create an array of information for the Agenda template
 * 
 */

function picto_create_news() {

	$current_post_id = get_the_ID();
	$exclude_id[] = $current_post_id;
	 
	 // IMAGES
	  
	 $img_info = gallery_init('medium');
	   		  					 
	 if ( empty( $img_info ) ) { 
	 	$img_url_custom = '';
	 	$img_url_large = '';
	 } else { // not empty!
	 	$img_url_custom = $img_info[0]["url-custom"];
	 	$img_url_large = $img_info[0]["url-large"];
	 }
	 
	 // CONTENT
	 
	 // used to be : "content" => get_the_content(),
	 
	  $picto_content = apply_filters(
	  	'the_content', 
	  	get_the_content(
	  		sprintf(
	 	 			__( 'Continue reading %s', 'twentyfifteen' ),
	 	 			the_title( '<span class="screen-reader-text">', '</span>', false )
	 	 		) 
	 	 	) // get_the_content
	 	 );
	 
	 // Remove hyperlinks, to avoid nested hyperlinks.
	 
	 $picto_content = picto_disable_hyperlinks( $picto_content );
	 	 
	 // RELATED ARTISTS
	 
	 $related_artists = get_field('artistes');
	 
	 // EXTERNAL ARTISTS
	 
	 $external_artists = get_field('artistes_exposants');
	 	
	 // DATE
	
	 $mem_date = mem_date_processing( 
	 	get_post_meta($current_post_id, '_mem_start_date', true) , 
	 	get_post_meta($current_post_id, '_mem_end_date', true)
	 );
	 
	 /*
	  * Prepare date values.
	  * Event date or publication date?
	 */
	 
	 if ($mem_date["start-iso"] !="" ) { 
	 
	 			// case 1: use MEM date
	 			
	 					// $date_start_raw = get_post_meta($current_post_id, '_mem_start_date', true);
	 				
	 					$date_string = $mem_date["date"];
	 					$date_short = $mem_date["date-short"];
	 					$date_num = $mem_date["date-num"];
	 					$date_year = $mem_date["start-year"];
	 					
	 					$start_date_iso = $mem_date["start-iso"];  // 2013-03-11T06:35
	 					$end_date_iso = $mem_date["end-iso"];
	 					$unix_start = $mem_date["start-unix"];
	 					$unix_end = $mem_date["end-unix"];
	 					$has_event_date = true;
	 			 
	   } else {
	   
	   	// case 2: no MEM date defined - use POST DATE as START DATE
	   				
	   				$date_string = get_the_date( 'l j F Y' ); // Mercredi 5 juin 2013
	   				$date_short = get_the_date( 'F Y' );
	   				$date_num = get_the_date( 'd.m.Y' );
	   				$date_year = get_the_date( 'Y' );
	   				
	   				$start_date_iso = get_the_date( 'Y-m-d\TH:i' );  // 2013-03-11T06:35
	   				$end_date_iso = $start_date_iso;
	   				$unix_start = strtotime( $start_date_iso );
	   				$unix_end = $unix_start;
	   				$has_event_date = false;
	   				
	   }
	 
	 $archive_array = array( 
	     	"id" => $current_post_id,
	     	"permalink" => get_permalink(), // NOTE: may return in form of /?p=191
	     	"slug" => get_post_field( 'post_name', $current_post_id ),
	     	"title" => get_the_title(),
	     	"content" => $picto_content,
	     	
	     	// IMAGES
	     	"url-custom" => $img_url_custom,
	     	"url-large" => $img_url_large,
	     	
	     	// DATES
	     	"date-string" => $date_string,
	     	"date-short" => $date_short,
	     	"date-num" => $date_num,
	     	"date-year" => $date_year,
	     	"date-pub" => get_the_date( 'l j F Y' ),
	     	"start-date-iso" => $start_date_iso,
	     	"end-date-iso" => $end_date_iso,
	     	"start-date-unix" => $unix_start,
	     	"end-date-unix" => $unix_end,
	     	"has-event" => $has_event_date,
	     	
	     	// PERSON
	     	 "related-artists" => $related_artists,
	     	 "external-artists" => $external_artists,
	     	 
	     	 // DEBUG
	     	 // "date-start-raw" => $date_start_raw,
	     	
	  );
	  
	  return $archive_array;
	  
}

/*
 * Get Year of post -
 * Based on event date or publication date.
*/

function picto_get_year( $current_post_id ) {

		$mem_date = mem_date_processing( 
			get_post_meta($current_post_id, '_mem_start_date', true) , 
			get_post_meta($current_post_id, '_mem_end_date', true)
		);
		
		if ($mem_date["start-iso"] !="" ) { 
		
					// case 1: use MEM date
					$date_year = $mem_date["start-year"];

		  } else {
		  
		  	// case 2: no MEM date defined
		  	// use POST DATE as START DATE
		  	$date_year = get_the_date( 'Y', $current_post_id  );
		  	
		  }
		  
		 return $date_year;
		
}


function picto_categories() {

	$item_cat = get_the_category();
	$picto_cat = '';
	
//				 echo '<pre>';
//				 var_dump($item_cat);
//				 echo '</pre>';
	
	foreach( $item_cat as $cat ) {
	
			if ( $cat->slug != 'agenda' ) {
				
				$term_link = get_term_link( $cat );
				
				$picto_cat .= '<a href="'. $term_link .'">';
				$picto_cat .= $cat->name;
				$picto_cat .= '</a> / ';
			
			}
	}
	
	return $picto_cat;

}

function picto_related_artists( $item, $context ) {
	
	$related_artists = $item["related-artists"];
	
	$external_artists = $item["external-artists"];
	
	$output = '';
	
//	 							 echo '<pre>';
//	 							 var_dump($external_artists);
//	 							 echo '</pre>';
	
	if ($related_artists) { 
	
		$output = '<div class="related-artists">';
	
	} else if ( $external_artists != '' ) {
	
		$output = '<div class="related-artists">';
	
	}
	
			
			if ( $related_artists ) {
			
					$howmany = count( $related_artists );
					$counter = 1;
					
					 foreach ( $related_artists as $id ) {
					 	
					 	$custom_query = new WP_Query( array(
										'post_type' => 'membres',
										'page_id' => $id
								) ); 
					 	      			
					 	      		if ($custom_query->have_posts()) : 
					 	      		
					 	      		while( $custom_query->have_posts() ) : $custom_query->the_post();
					 	      				
					 	      				 // caractère de séparation...
					 	      				 
					 	      				 if ( $howmany > 1 ) {
					 	      				 			
					 	      				 			if ( $counter == $howmany ) {
					 	      				 				 
					 	      				 				$output .= ' & ';	
					 	      				 				 
					 	      				 			} else if ( $counter > 1 ) {
					 	      				 		 		
					 	      				 		 		$output .= ', ';	
					 	      				 		 		
					 	      				 		 }
					 	      				 }
					 	      				 
					 	      				 if ( $context == 'single' ) {
					 	      				 
					 	      				 	$output .= '<a href="' . get_the_permalink() . '">';
					 	      				 
					 	      				 }
					 	      				 
					 	      				 $output .= picto_nom_prenom( get_the_title() );
					 	      				 	
					 	      				 	if ( $context == 'single' ) {
					 	      				 	
					 	      				 		$output .= '</a>';
					 	      				 		
					 	      				 	}
					 	      				
					 	      				$counter++;
					 	
					 		       	endwhile;
					 	      		endif;
					 	     wp_reset_postdata();
					 	     
					 	     } // foreach
					 			
					 			
			 			
			 			} // END if $related_artists
			 			
			 			
			 			if ( $external_artists != '' ) {
			 					
			 					$output .= ' ' . $external_artists ;
			 			
			 			} // END if $external_artists
			 			
			 			
			 			if ($related_artists) { 
			 			
			 				$output .= '</div>';
			 			
			 			} else if ( $external_artists != '' ) {
			 			
			 				$output .= '</div>';
			 			
			 			}
	 			
	 			
	 			return $output;
	
}

function picto_echo_news( $item, $context ) {
 				
 				$item_header_class = '';
 				
 				if (empty($item["url-custom"])) {
 					$item_header_class .= ' no-img';
 				} else { 
 					$item_header_class .= ' has-img';
 				}

 				echo '<div class="news-item'.$item_header_class.'">';
 				
 					// Build URL
 					// Adresse web de WordPress (URL) = WordPress Address (URL) = get_site_url()
 					// Adresse web du site (URL) = Site Address (URL) = get_home_url
 					
 					// Note 1: $item["permalink"]; ne fonctionne pas bien pour les articles planifiés
 					
 					// Note 2: sur les sous pages (contexte: 'sub-page'), il faut prendre l'URL originale, 
 					// car elle contient un sous-élément: 
 					// p.ex. http://pictonet.ch/hors-cases/galerie-1-piece/
 					// ou http://pictonet.ch/residences/residence-curatoriale/
 					
 					if ( $context == 'sub-page' ) {
 					
 						$news_url = $item["permalink"];
 					
 					} else {
 						
 						$news_url = get_home_url().'/'.$item["slug"].'/';
 					
 					}
 					
 				echo '<a class="news-item-link entry-header" href="'.$news_url.'">';
 				
 							/** 
 							 * show the image.
 							 */
 							
 							if ( $item["url-custom"] ) {
 									
 									echo '<div class="news-item-img"><img src="'.$item["url-custom"].'"></div>';
 			
 							}
 							
 							 ?>
 							<h2 class="news-item-title entry-title <?php 
 							
 							if (!empty($item["person-link"])) {
 							
 								echo ' norm';
 							}
 							
 							 ?>"><?php 
 							 
 							 echo $item["title"];
 							 
 							 ?></h2>
 							 
 							 <?php 
 							 
 							 // check for related artists ...
 							  							 
 							 	echo picto_related_artists( $item, 'agenda' );
 							 	
 							// Test if a publication date exists.
 							if ( $item["has-event"] == true ) {
 											
 									?>
 					<div class="event-date bold">
 					
	 							<time itemprop="datePublished" datetime="<?php echo $item["start-date-iso"] ?>">
	 							<?php 
					
										echo $item["date-num"]; 
										
										// DEBUG: echo ' ('.$item["date-start-raw"].' = '.$item["start-date-iso"].')';
 							
 							 ?>
 							</time>
 					</div><!-- event-date-box -->
 							<?php
 						
 						} // end testing for Pub-Date
 						
 						
 						if ( $context == 'sub-page' ) {
 						
 							// display content!
 							
 							echo '<div class="entry-content">';
 							
 							echo $item["content"]; 
 							
 							echo '</div>';
 						
 						}
 						
 						
 					?>
 					</a><!-- news-item-title -->
 			</div><!-- news-item -->
 
 	<?php
 
 }
 

function picto_read_more_link() {
   return '<div class="read-more">Lire la suite</div>';
//     return '';
}
add_filter( 'the_content_more_link', 'picto_read_more_link' );


