<?php

function picto_hyperlinks( $otherlinks ) {
	
	$links = '';
	
	if ($otherlinks) {
	
		$links .= '<div class="hyperlinks links-other">';
		$links .= '<ul class="clean">';
		 	 
		 foreach ( $otherlinks as $otherlink ) {
		 
				 if ( !empty($otherlink) ) {
				 			
				 			// test if it starts with HTTP:
				 			
				  	 	$linkstart = substr( $otherlink, 0, 4 );
				  	 	if ( $linkstart != "http" ) {
				  	 		$otherlink = 'http://'.$otherlink;
				  	 	}
				  	 	
				  	 	$humanlink = $otherlink;
				  	 	
				  	 	// remove HTTP to make it look nicer
				  	  $humanlink = str_replace("http://", "", $humanlink);
				  	 	$humanlink = str_replace("https://", "", $humanlink);
				  	 	
				  	 	// remove www.
				  	 	$humanlink = str_replace("www.", "", $humanlink);
				  	 	
				  	 	// Remove whatever is after the next "/"
				  	 	
				  	 	$humanlink = explode( "/", $humanlink);
				  	 	$humanlink = $humanlink[0];
				  	 	
				  		$links .= '<li class="link"><a class="url" href="'.$otherlink.'">'.$humanlink.'</a></li>';
		  
		  		 } // if
			 } // foreach
			 
			 $links .= '</ul>';
			$links .= '</div><!-- .hyperlinks -->';
	
	} // end if $otherlinks
	
	return $links;
	
}


