<?php

/*
 * picto_prevnext_byname()
 **************************
 *
 * Produce previous / next links, based on the NAME rather than publication date.
 *
 * Required input: 
 * - ID of current post = $current_post_id
 *
 * Returns an array with the following keys:
 * - [prev-url]
 * - [prev-name]
 * - [next-url]
 * - [next-name]
 *
 * Uses picto_members_query(), also used for the Sidebar.
 *  
 */
 
function picto_prevnext_byname( $current_post_id ) {
 		
 		$prevnext = array();
 		
 		$array_membres = picto_members_query();
 						
		$array_count = count($array_membres);
		
		$last_key = $array_count -1;

		$current_key = picto_array_search(
				$array_membres, // $array
				'id', // $field
				$current_post_id // $value
			);
		// now, $current_key is the NUMBER of current array position.
		
		// Define PREV Item
		
		if ( $current_key == 0 ) {
			// echo '<p>First item!</p>';
			$prev_key = $last_key;
		} else {
			$prev_key = $current_key - 1;
		}
		
		$prevnext["prev-url"] = $array_membres[$prev_key]["permalink"];
		$prevnext["prev-name"] = $array_membres[$prev_key]["title"];
		
		// Define NEXT Item
		
		if ( $current_key == $last_key ) {
			// echo '<p>Last item!</p>';
			$next_key = 0;
		} else {
			$next_key = $current_key + 1;
		}
		
		$prevnext["next-url"] = $array_membres[$next_key]["permalink"];
		$prevnext["next-name"] = $array_membres[$next_key]["title"];
		
		return $prevnext;
 	
}


/*
 * picto_prevnext_bydate()
 **************************
 *
 * Produce previous / next links, based on both event date and publication date.
 *
 * Required input: 
 * - ID of current post = $current_post_id
 * - current year
 *
 * Returns an array with the following keys:
 * - [prev-url]
 * - [prev-name]
 * - [next-url]
 * - [next-name]
 *
 * - Uses picto_archive_query( $current_year )
 * 
 */

function picto_prevnext_bydate( $current_post_id, $current_year ) {
 		
 		$prevnext = array();
 		
 		$array_posts = picto_archive_query( $current_year );
 		
		$array_count = count($array_posts);
		
		if ( $array_count > 1 ) { // Check if there's more than 1 item
			$last_key = $array_count - 1;
		} else { $last_key = 0; }
		
		$current_key = picto_array_search(
				$array_posts, // $array
				'id', // $field
				$current_post_id // $value
			);
			
		// $current_key is the NUMBER of current array position.
		// Items are defined from NEWEST to OLDEST.
		// We may also need to parse the previous and next year.
		
		// Define PREV Item
		// ********************
		
		if ( $current_key == $last_key ) {
		
			// echo '<p>Oldest item!</p>';
			// item is oldest in list - we need to check for an older array.
			
			$array_previous_year = picto_archive_query( $current_year - 1 );
				 		
			// load newest (key = 0) item of previous year.
			if ( !empty($array_previous_year) ) {
				$prevnext["prev-url"] = $array_previous_year[0]["permalink"];
				$prevnext["prev-name"] = $array_previous_year[0]["title"];
			}
			
		} else {
			// Easy, just define prev item
			$prev_key = $current_key + 1;
			$prevnext["prev-url"] = $array_posts[$prev_key]["permalink"];
			$prevnext["prev-name"] = $array_posts[$prev_key]["title"];
		}
		
		// Define NEXT Item
		// ********************
		
		if ( $current_key == 0 ) {
			
			// item is newest in list - we need to check for a newer array.
			
			$array_next_year = picto_archive_query( $current_year + 1 );
			
			// load oldest (key = max) item of Next year.
			if ( !empty($array_next_year) ) {
			
				$oldest = count($array_next_year);
				$oldest = $oldest - 1; // because numbering starts with 0
				
				$prevnext["next-url"] = $array_next_year[$oldest]["permalink"];
				$prevnext["next-name"] = $array_next_year[$oldest]["title"];
			}
						
		} else {
			// Easy, just define next item
			$next_key = $current_key - 1;
			$prevnext["next-url"] = $array_posts[$next_key]["permalink"];
			$prevnext["next-name"] = $array_posts[$next_key]["title"];
		}
		
		// DONE!
		
		return $prevnext;
 	
 }
 
 /*
  * Function that produces previous / next links  (output).
  *
  * Required input: array with the following keys:
  * - [prev-url]
  * - [prev-name]
  * - [next-url]
  * - [next-name]
  */
 
function picto_prevnext_output( $prevnext, $context ) {
 		
 		$output = '<nav class="navigation post-navigation" role="navigation">';
 		$output .= '<h2 class="screen-reader-text">Navigation de l’article</h2>';
 		
 		$output .= '<div class="nav-links">';
 		
 		// Check if previous is defined 
 		
 		if ( !empty($prevnext["prev-url"]) ) {
 			
 			if ( $context == 'membres' ) {
 				$prevnext["prev-name"] = picto_nom_prenom( $prevnext["prev-name"] );
 			}
 			
	 		$output .= '<div class="nav-previous">';
	 		$output .= '<a href="'. $prevnext["prev-url"] .'" rel="prev">';
	 		
	 		$output .= '<span class="meta-nav" aria-hidden="true">&lt;</span> <span class="screen-reader-text">Article précédent&nbsp;:</span> <span class="post-title">';
	 		$output .= $prevnext["prev-name"];
	 		if ( empty($prevnext["prev-name"]) ) {
	 			$output .= 'Article précédent';
	 		}
	 		$output .= '</span></a></div>';
 		
 		}
 		
 		// Check if next is defined
 		
 		if ( !empty($prevnext["next-url"]) ) {
 				
 				if ( $context == 'membres' ) {
 						$prevnext["next-name"] = picto_nom_prenom( $prevnext["next-name"] );
 					}
 				
		 		$output .= '<div class="nav-next">';
		 		$output .= '<a href="'. $prevnext["next-url"] .'" rel="next">';
		 		$output .= '<span class="meta-nav" aria-hidden="true">&gt;</span> <span class="screen-reader-text">Article suivant&nbsp;:</span> <span class="post-title">';
		 		
		 		$output .= $prevnext["next-name"];
		 		if ( empty($prevnext["next-name"]) ) {
		 				$output .= 'Article suivant';
		 			}
		 		$output .= '</span></a></div>';
 		
 		}
 		
 		$output .= '</div>';
 		$output .= '</nav>';

 		
 		return $output;
 		
}
 