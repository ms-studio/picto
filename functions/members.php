<?php 


// Helps to order array by string
function picto_multi_array_sort($a,$b) {
     //return $a['title']>$b['title'];
     return strcmp($a["lowercase"], $b["lowercase"]);
}

// PHP Multidimensional Array Searching (Find key by specific value)
// Source: http://stackoverflow.com/a/8102246/1904769
function picto_array_search($array, $field, $value) {
   foreach($array as $key => $array)
   {
      if ( $array[$field] === $value )
         return $key;
   }
   return false;
}

/*
 * picto_members_query
 *********************
 *
 * Permet de construire la liste des membres.
 * Retourne un array() avec les clés suivantes:
 * - permalink
 * - title
 * - lowercase
 *
 * Cette fonction est utilisée sur:
 * - la section membres (cf content-memberlist.php)
 * - la navigation entre pages membres (cf content-members.php)
 *
*/

function picto_members_query() {

		if ( is_user_logged_in() ) {
		
				  		    delete_transient( 'page_membres' );
		
		}
				  		
		if ( false === ( $array_membres = get_transient('page_membres') ) ) {
		
	      		$array_membres = array();
	      				
	  				$custom_query = new WP_Query( array(
		      		 		'post_type' => array( 'membres' ),
		      		 		'posts_per_page' => -1,
		      		 		'orderby'  => 'title',
		      		 		'order'  => 'ASC',
		      		 		));
		      		
		      		if ( $custom_query->have_posts() ) :
		      				  	while( $custom_query->have_posts() ) : $custom_query->the_post();
		      				  	
		      				  		// put into array.
		      				  		$array_membres[] = array( 
		      				  		    	"id" => get_the_ID(),
		      				  		    	"permalink" => get_permalink(),
		      				  		    	"title" => get_the_title(),
		      				  		    	"lowercase" => strtolower( get_the_title() ),
		      				  		 );
		      				  	
		      				  	endwhile; 
		      		endif;

		      		// 3:
		      		// Order the array, based on the "lowercase" field
		      		
		      		usort($array_membres, "picto_multi_array_sort");
		      		
		      		// 4:
		      		// put into transient
		      		
		      		set_transient( 'page_membres', $array_membres, 12 * HOUR_IN_SECONDS  ); 
		      		// 12 * HOUR_IN_SECONDS 
  		
  		} // end testing for transient
  		
  		// return the result
  		
  		return $array_membres;

}

function picto_members_output() {
	
	$output = '';
	
	// Query for post type = "membre":
		
	$array_membres = picto_members_query();

  		if ( !empty ($array_membres) ) :
  				  	
  				  	$initialcounter = 1;
  				  	$prev_initiale = '';

  						  foreach ($array_membres as $key => $item) {
  						  
      						  // Tester l'initiale.
      						  
      						  $nom_entier = $array_membres[$key]["title"] ;
      						  $nom_initiale = strtolower(mb_substr($nom_entier,0,1, "utf-8"));
      						  
      						  if (in_array($nom_initiale, array("a","b"))) {
      						  		$nom_initiale = "a-b";
      						  }
      						  
      						  if (in_array($nom_initiale, array("e","f"))) {
      						  		$nom_initiale = "e-f";
      						  }
      						  
      						  if (in_array($nom_initiale, array("g", "h"))) {
      						  		$nom_initiale = "g-h";
      						  }
      						  
      						  if (in_array($nom_initiale, array("i","j"))) {
      						  		$nom_initiale = "i-j";
      						  }
      						  
      						  if (in_array($nom_initiale, array("k","l"))) {
      						  		$nom_initiale = "k-l";
      						  }
      						  
      						  if (in_array($nom_initiale, array("m","n"))) {
      						  		$nom_initiale = "m-n";
      						  }
      						  
      						  if (in_array($nom_initiale, array("o","p"))) {
      						  		$nom_initiale = "o-p";
      						  }
      						  
      						  if (in_array($nom_initiale, array("t","u"))) {
      						  		$nom_initiale = "t-u";
      						  }
      						  
      						  if (in_array($nom_initiale, array("v","w","x","y","z"))) {
      						  		$nom_initiale = "v-w-y-z";
      						  }
      						  		
      						  		if ($nom_initiale != $prev_initiale) {
      						  					      						  		
      						  			if ($initialcounter != 1) {
      						  				$output .= '</ul></div>'; // close previous initiale
      						  			}
      						  			
      						  			$output .= '<div class="bloc-initiale">';
      						  			
      						  			$output .= '<h2 class="h2 initiale" id="noms-'. $nom_initiale .'"><span class="initiale-text">'. $nom_initiale .'</span></h2>';
      						  				
      						  			$output .= '<ul class="ul-initiale clean unstyled rel">';
      						  		} 
      						  
      						   $output .= '<li class="li"><a href="'; 
      						   
      						   $output .= $array_membres[$key]["permalink"];
      						   
      						   $output .='">';
      						   
      						   $output .= picto_nom_prenom($nom_entier);
      						   
      						   $output .= '</a></li>';
  				  			
  				  			// increment counter
  				  			$prev_initiale = $nom_initiale;
  				  			$initialcounter++;
  				  
  				  		} 
  				  		
  				  	// close the final "bloc-initiale"
  				  	$output .= '</ul></div>';

  		endif;
	  			
	return $output;
	
}