# Emails

Adresses email configurées:

Email principal du site: wpadmin@pictonet.ch  
Redirections: ms@ms-studio.net, retocrameri@gmail.com

Webmaster: webmaster@pictonet.ch  
Redirections: aucune (webmail only)

Newsletter Mailpoet: picto@pictonet.ch - À CRÉER!!   
Gestion des retours: retours@pictonet.ch

## Newsletter

La newsletter utilise le service Sendgrid.

user: picto
email: wpadmin@pictonet.ch 

Un SPF a été mis en place pour le sous-domaine newsletter.pictonet.ch

## SPF général

Un réglage SPF général pour les emails pictonet.ch

v=spf1 ip4:94.103.96.173/24 a mx include:sendgrid.net ~all

