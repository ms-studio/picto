# Plugins

Les extensions WordPress importantes pour le site:

### Advanced Custom Fields PRO

Fait fonctionner les champs de contenu supplémentaires: Galeries d'images, Liens...

### Minimalistic Event Manager

Fait fonctionner la gestion des dates d'événement de l'agenda.

Autres: 

* Admin Color Schemes
* Admin Trim Interface
* Disable Comments = permet de désactiver les commentaires partout.
* Formidable
* Fotorama = voir https://wordpress.org/plugins/fotorama/installation/
* Grey Admin Color Schemes