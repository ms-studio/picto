# Logique d'affichage de l'agenda

On trouve des vues agenda dans différents endroits du site:

- **Page d'accueil**: page statique utilisant le template page-templates/agenda.php
- **Archive par année**: archive.php
- **Pages**: avec une option ACF, on peut choisir d'afficher en pied de page un agenda par catégorie. Utilisé pour [Topic](http://pictonet.ch/hors-cases/topic-espace-dart-independant/) et [Artiste en résidence](http://pictonet.ch/residences/residence-artistique/).
- **Template sous-pages**: ce template utilise le même code, même si ce n'est pas un agenda.

## La méthode: 

Dans tous ces cas, on utilise une même méthode:

1. préparation des données: on crée d'abord un array contenant toutes les news.
2. on affiche le résultat avec picto_echo_news( $item, 'agenda' );

### Préparation des données: 

Elle peut s'effectuer avec différentes fonctions:

- Dans l'agenda par année: picto_archive_query( $current_year )
- Dans le template agenda: picto_archive_query( date("Y") )
- Dans l'agenda par catégorie: picto_cat_query( $acf_agenda_cat )

Note: picto_archive_query() effectue deux requêtes, l'une pour les articles avec une date d'événement, l'autre pour ceux qui n'en ont pas. Les résultats sont ensuite combinés.

Dans tous les cas, une fois la requête effectuée, on utilise la même fonction -
picto_create_news() - pour générer le contenu d'un array, qui sera ensuite affiché avec picto_echo_news().

### Affichage des données:

Dans tous les cas, une même fonction est utilisée pour générer l'affichage, avec une variable donnant le contexte.

La fonction : picto_echo_news( $item, $context )

Voici les contextes en utilisation:

- Dans l'agenda par année = picto_echo_news( $item, 'archive' );
- Dans le template Agenda = picto_echo_news( $item, 'agenda' );
- Dans l'agenda par catégorie = picto_echo_news( $item, 'agenda-cat' );
- Dans les sous-pages = picto_echo_news( $item, 'sub-page' );

## Logique détaillée du template Agenda

Dans ce template, on utilise plusieurs requêtes dont on combine le résultat. 

Cela est nécessaire, car la fonction picto_archive_query() ne prend en compte qu'une année. Pour l'agenda, on souhaitera souvent afficher aussi des événements de l'année précédente ou suivante.

La logique: 

- Si on est dans le mois de décembre, on va aussi prendre en compte l'année à venir.
- Jusqu'au mois de juin, on va aussi prendre en compte l'année précédente.
- On se limite à des événements situés 30 jours dans le futur.
- On limite le nombre total d'événements (à 50).

***