# Fonctionnement du carousel

Le carousel de la page d'accueil.

Le code se trouve dans page-templates/agenda.php - c'est le template utilisé pour la page d'accueil.

**Est-ce que le carousel utilise jQuery?**

Oui, pour l'effet de fondu lors de la transition.

