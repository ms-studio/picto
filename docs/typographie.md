# Typographie

Organisation des fontes:

- Regular (texte courant)
- Light (liste des noms)
- Condensed #18 (sous-titres, formulaire)
- Bold #2 (quels endroits ?)
- Bold Condensed #20 (titres, menu)
- Oblique

Les fontes de base, les plus utilisées:

- **Regular** (texte courant)
- **Bold Condensed #20** (titres, menu)

Ces deux fontes sont chargées dans le CSS principal.

Les autres variantes vont être chargées par le CSS secondaire.

Fontes de base, chargées dans css/dev/10-fonts.css :

- **Regular** = font-family: 'TGregular';
- **Condensed Bold** = font-family: 'TGcnd'; font-weight: bold;

Fontes secondaires, chargées dans fonts/fonts.css :

- **Light** = font-family: 'TGregular'; font-weight: 200;
- **Condensed Regular** = font-family: 'TGcnd'; font-weight: 400;
- **Bold** = font-family: 'TGregular'; font-weight: 700;

Note: la fonte italique est buggy (mauvais espacement des glyphes), c'est pourquoi on ne la charge pas.

***