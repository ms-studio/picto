# Médias

La taille recommandée pour les images: 1200 pixels de taille horizontale ou verticale.

Réglages des médias - Tailles des images:

- **Miniatures:** 500 x 500, (recadrage: oui)
- **Taille moyenne:** 1200 x 1200
- **Grande taille:** 2400 x 2600

## Les galeries

Les galeries sont crées par la fonction `picto_slideshow()`, invoquée dans `content.php`, et définie dans functions/galleries.php.

Le prinicpe: 

1. On obtient la liste des images choisies par l'interface ACF.
2. On crée un shortcode [gallery] avec les ID de ces images.
3. Le plugin [Fotorama](https://wordpress.org/plugins/fotorama/) traite le shortcode et produit la galerie.

La taille d'image utilisée par Fotorama est la taille moyenne (medium).