# Foire Aux Questions

## Corriger un copier-coller

Il peut arriver qu'un style de texte copié (depuis Word) prenne le dessus sur le style du site : la fonte n'est plus à la bonne taille, ou se présente dans une autre police.

On peut corriger cela de différentes manières:

- En insérant le texte dans le mode "Texte" plutôt que "Visuel". Dans ce cas, aucun formatage ne sera retenu.
- En mode visuel, la barre d'outils contient l'élément "Nettoyer le formatage", symbolisé par une gomme. En sélectionnant le texte, puis cliquant sur ce bouton, on supprime les styles importés.

Voir le chapitre [Texte et formatage](http://cours-web.ch/wp/texte-formatage) du support de cours pour plus de détails.

## Styles de formulaires

Pour avoir les styles de formulaires du site Picto, il faut indiquer dans les réglages du formulaire qu'on ne veut *pas* charger les styles Formidable.

Pour chaque formulaire, on a l'option d'utiliser:

- les styles par défaut de Formidable ("base").
- pas de style du tout ("style désactivé").

Dans le deuxième cas, ce sont les styles du thème Picto qui seront appliqués. Lors de l'édition du formulaire, se rendre dans "Paramètres", puis "Style & Boutons" pour faire ce réglage.

On peut gérer cela globalement pour plusieurs formulaires, en allant dans "Formulaires > Styles > [Gérer les styles Formulaire](http://pictonet.ch/wp-admin/admin.php?page=formidable-styles&frm_action=manage)": ici on pourra sélectionner "style désactivé" pour les formulaires voulus.