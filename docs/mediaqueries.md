# Media Queries

Les breakpoints responsive du thème Twentyfifteen.

Le thème possède 7 breakpoints:

 * 16.1 - Mobile Large - 620px
 * 16.2 - Tablet Small - 740px
 * 16.3 - Tablet Large - 880px
 * 16.4 - Desktop Small - 955px
 * 16.5 - Desktop Medium - 1100px
 * 16.6 - Desktop Large - 1240px
 * 16.7 - Desktop X-Large - 1403px


- iPhone 5 portrait: 320
- Samsung Galaxy S6 portrait: 360

0) 480px = le contenu passe de 1 à 2 colonnes.

- iPhone 5 landscape: 568

 
1) Mobile Large 620px     
(min-width: 38.75em)

- Samsung Galaxy S6 landscape: 640
- iPhone 6 landscape: 667

2) Tablet Small 740px     
(min-width: 46.25em)

- iPad retina portrait : 768

3) Tablet Large 880px     
(min-width: 55em)

4) Desktop Small 955px     
(min-width: 59.6875em)

= the limit where the menu switches to sidebar!

here, the layout is defined:

```
.sidebar {
		float: left;
		margin-right: -100%;
		max-width: 413px;
		position: relative;
		width: 29.4118%;
	}
	
.site-content {
    display: block;
    float: left;
    margin-left: 29.4118%;
    width: 70.5882%;
}
```

Note: la taille de la fonte est réduite.


5) Desktop Medium 1100px     
(min-width: 68.75em)

6) Desktop Large 1240px     
(min-width: 77.5em)

- MacBookPro 13inch: 1280

7) Desktop X-Large 1403px     
(min-width: 87.6875em)

- MacBookPro 15inch: 1440

***

# Système de Grille 

Système utilisé: http://salvattore.com/

Le passage à deux colonnes se fait dans les mediaqueries, à la transition "480px = 30em", de cette manière:

```css
#grid[data-columns]::before {
  content: '2 .column.size-1of2';
}
```

***

Tailles écran ressortant de Google Analytics:

* 1440 × 900 -  36 (16.00%) - MacBookPro 15inch
* 1920 × 1080 - 27 (12.00%)
* 1280 × 800 -  35 (15.56%) actually 42 (18%)... -  MacBookPro 13inch (can scale to 1440x900 and 1680x1050)
* 1920 × 1200 - 10 (4.44%)
* 320 × 568 -   24 (10.67%) - iPhone 5
* 375 × 667 -   13 (5.78%) - iPhone 6
* 2560 × 1440 - 19 (8.44%) - QuadHD
* 1680 × 1050 - 10 (4.44%)
* 360 × 640 -   11 (4.89%) - Samsung Galaxy S6
* 1280 x 1024 - 7 (3.11%)

iPad retina: 768 x 1024

Nokia Lumia 1020 (2013):
1280 × 768 px

* 960 × 540 = Quarter HD (qHD) - HTC Desire 601, Samsung Galaxy S4 Mini, Sony Xperia M2...
* 1280 × 720 = 720p HD
* 1920 × 1080 = Full HD (1080p)
* 2560 × 1440 is Quad HD (WQHD, 1440p)
* 3200 ×1800 - Dell XPS 13 (2015) Ultrabook = Quad HD +
* 3840 × 2160 =  4K, or 4K Ultra HD (UHD, 4K)

Device size ressources:

*  https://material.io/devices/

***