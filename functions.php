<?php 

// Change-Detector-XXXXXXXXXXXXXXXXXXXXXXXXX - for Espresso.app


add_action( 'after_setup_theme', 'my_child_theme_setup' );
function my_child_theme_setup() {
    
			load_theme_textdomain( 'twentyfifteen', get_stylesheet_directory() . '/languages' );
    
}


function picto_register_styles() {

	/**
	 * Custom CSS
	 */
	 
	$picto_dev_mode = false;
	
	if ( current_user_can('edit_others_pages') ) {
		 $picto_dev_mode = true;
	} else {
		
		$host = $_SERVER['HTTP_HOST'];
		
		if ( $host != 'espacepicto.ch' ) {
			 $picto_dev_mode = true;
		}
		
	}
	
	wp_enqueue_style( 
		'picto-parent-style', // $handle
		get_template_directory_uri() . '/style.css' // $src 
	);
	
	if ( $picto_dev_mode == true ) {
	
			// DEV: the MAIN stylesheet - uncompressed
			wp_enqueue_style( 
					'main-style', 
					get_stylesheet_directory_uri() . '/css/dev/00-main.css', // main.css
					false, // dependencies
					strtotime("now") // version
			); 
	
	} else {
	
			// PROD: the MAIN stylesheet - combined and minified
			wp_enqueue_style( 
					'main-style', 
					get_stylesheet_directory_uri() . '/css/prod/styles.20170920065626.css', // main.css
					false, // dependencies
					null // version
			); 
	}
		
		
		wp_dequeue_style( 'twentyfifteen-style' );
		wp_deregister_style( 'twentyfifteen-style' );
		
		wp_dequeue_style( 'twentyfifteen-fonts' );
		wp_deregister_style( 'twentyfifteen-fonts' );
		
//		wp_dequeue_style( 'genericons' );
//		wp_deregister_style( 'genericons' );
		
		$webfont_tradegothic = '//fast.fonts.net/cssapi/5605c4b0-7667-43ae-b22c-b6edf854d511.css';
		$webfont_oswald = '//fonts.googleapis.com/css?family=Oswald:400,300';
		
		
//		wp_enqueue_style( 
//			'picto-font-style', // $handle
//			$webfont_tradegothic 
//		);

			$plugins_url = plugins_url();
			
			wp_enqueue_style( 
					'formidable-icons', 
					$plugins_url . '/formidable/css/font_icons.css', // main.css
					false, // dependencies
					'2.02.06' // version
			); 
			
			wp_enqueue_style( 
					'formidable-dropzone', 
					$plugins_url . '/formidable/pro/css/dropzone.css', // main.css
					false, // dependencies
					'2.03.03' // version
			); 
			
			
		
	wp_enqueue_script( 
			'main-script',
			get_stylesheet_directory_uri() . '/js/scripts.js', // scripts.js
			array('jquery'), // dependencies
			null, // version
			true // in footer
	);
	
	wp_enqueue_script( 
			'salvattore',
			get_stylesheet_directory_uri() . '/js/libs/salvattore.min.js', // scripts.js
			null, // dependencies
			'1.0.9', // version
			true // in footer
	);

}
add_action( 'wp_enqueue_scripts', 'picto_register_styles', 25 );


/*
 * Add Secondary Fonts to Footer
*/

function picto_footer_code() {

		$picto_secondary_fonts = get_stylesheet_directory_uri() . '/fonts/fonts.css' ;
    
    echo '<link rel="stylesheet" id="picto-fonts-css"  href="'.$picto_secondary_fonts.'" type="text/css" media="all" />';
    

}
add_action( 'wp_footer', 'picto_footer_code' );


/*
 * Google Maps Style
*/

function picto_google_map() {

		if( is_page('contact') ) { 
			?>
			
			
			<style type="text/css">
			            /* Set a size for our map container, the Google Map will take up 100% of this container */
			            #map {
			                width: 750px;
			                height: 500px;
			                max-width: 100%;
			                margin-bottom: 2em;
			            }
			        </style>
			
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAiO3XpgHs5gRn7YogmbJZKre92MWJNU8M"></script>
			        
			<script type="text/javascript">
			            // When the window has finished loading create our google map below
			            google.maps.event.addDomListener(window, 'load', init);
			        
			            function init() {
			                // Basic options for a simple Google Map
			                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
			                var mapOptions = {
			                    // How zoomed in you want the map to start at (always required)
			                    zoom: 14,
			
			                    // The latitude and longitude to center the map (always required)
			                    center: new google.maps.LatLng(46.211, 6.13), // Geneva
			
			                    // How you would like to style the map. 
			                    // This is where you would paste any style found on Snazzy Maps.
			                    styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
			                };
			
			                // Get the HTML DOM element that will contain your map 
			                // We are using a div with id="map" seen below in the <body>
			                var mapElement = document.getElementById('map');
			
			                // Create the Google Map using our element and options defined above
			                var map = new google.maps.Map(mapElement, mapOptions);
			
			                // Let's also add a marker while we're at it
			                var marker = new google.maps.Marker({
			                    position: new google.maps.LatLng(46.213007, 6.124616),
			                    map: map,
			                    title: 'Snazzy!'
			                });
			            }
			    </script>
					<?php	
		}
}

add_action( 'wp_footer', 'picto_google_map', 25 );


// Header Cleanup

remove_action('wp_head', 'wp_generator');


/*
 * Minimalistic Event Manager
*/

function my_mem_settings() {
	mem_plugin_settings( array( 'post' ), 'full' );
}
add_action( 'mem_init', 'my_mem_settings' );




// 
require_once('functions/utilities.php');
//
require_once('functions/galleries.php');
//
require_once('functions/agenda.php');

require_once('functions/content.php');

require_once('functions/members.php');

require_once('functions/prev-next.php');

