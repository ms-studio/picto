<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Post thumbnail.
		// twentyfifteen_post_thumbnail();
				
		$picto_slideshow = picto_slideshow();
		
		if ( !empty( $picto_slideshow ) ) {
		
			echo '<div class="content-header-gallery">';	
			echo $picto_slideshow;
			echo '</div>';
		
		}
		
	?>
	
	<header class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
			
			$item = picto_create_news();
			
			// Related Artists
			
			 echo picto_related_artists( $item, 'single' );
			
			// Category and Date
			
			// Get categories:
			
			$picto_cat = picto_categories();
			
			$picto_event_meta = false;
			
			if (!empty( $picto_cat )) {
			
				$picto_event_meta = true;
			
			} else if ( $item["has-event"] == true ) {
			
				$picto_event_meta = true;
			
			}
			
			if ( $picto_event_meta == true )  {
			
				echo '<div class="event-date bold">';
			
			}
			
			echo $picto_cat;
			
			// Test if event date exists.
			
			if ( $item["has-event"] == true ) {
										
					?>
							<time class="entry-date published updated" itemprop="datePublished" datetime="<?php echo $item["start-date-iso"] ?>">
							<?php 
			
								echo $item["date-num"]; 
						
						 ?>
						</time>
				
						<?php
				} // end testing date
				
			if ( $picto_event_meta == true )  {
			
				echo '</div><!-- event-date-box -->';
			
			}
			
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );
			
			// Links 
						
			$otherlinks = array();
						
			if( have_rows('acf_lien_externe_bloc') ):
				
					while( have_rows('acf_lien_externe_bloc') ): the_row(); 
											
					$otherlinks[] = get_sub_field('acf_lien_externe');
				
					endwhile;
						
				endif; 
			
			if ( $otherlinks ) {
			
				echo '<div class="meta-fields">';
						if ($otherlinks) {
							   echo picto_hyperlinks( $otherlinks );
						} 
				  echo '</div>';  
				}
			
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
