<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	
		// Post thumbnail.
		
		$picto_slideshow = picto_slideshow();
		
		if ($picto_slideshow) {
		
			echo '<div class="content-header-gallery">';	
			echo $picto_slideshow;
			echo '</div>';
			
		} else {
		
			twentyfifteen_post_thumbnail();
		
		}
		
		
		$updated = get_the_date( 'Y-d-m' );
		
	?>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title updated published" datetime="'. $updated .'">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php 
		
		// $picto_content = vldr_process_hyperlinks( get_the_content() );
		
		$picto_content = apply_filters( 'the_content', get_the_content() );
		
		$picto_content = vldr_process_hyperlinks( $picto_content );
		
		echo $picto_content;
		
		// the_content(); 
		
		
		// Links 
		
		$external_links =  get_post_meta($post->ID, 'acf_lien_externe_bloc', false);
		
		$otherlinks = array();
		
		if ( !empty($external_links) ) {
			  
			  // has_sub_field() = This function is outdated. Please use the have_rows() function instead.
			  // This function checks to see if a field (such as a Repeater or Flexible Content field) has any rows of data to loop over.
		
				if( have_rows('acf_lien_externe_bloc') ):
				
					while( have_rows('acf_lien_externe_bloc') ): the_row(); 
				
					 $otherlinks[] = get_sub_field('acf_lien_externe');
				
					endwhile;
						
				endif;   
	
		}
		
		if ( $otherlinks ) {
		
			echo '<div class="meta-fields">';
					if ($otherlinks) {
						   echo picto_hyperlinks( $otherlinks );
					} 
			  echo '</div>';  
			}
								
		
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->

<?php 

		// Generate Agenda!
		
		$acf_agenda_cat =  get_post_meta($post->ID, 'acf_agenda_cat', true);	
		
		$acf_agenda_cat_title = get_post_meta($post->ID, 'acf_agenda_cat_title', true);
		
		if ( $acf_agenda_cat != '' ) {
		
			// echo '<p>generate agenda for: cat '.$acf_agenda_cat.'</p>';
		
			// produce a query, similar to archive page but with category.
			
			$news_array = picto_cat_query( $acf_agenda_cat );
			
			if ($news_array) {
					
					if ($acf_agenda_cat_title != '' ) {
						echo '<h2 class="page-header sub-agenda entry-title">'.$acf_agenda_cat_title.'</h2>';
					}
					
					echo '<div id="grid" class="grid sub-agenda-grid hentry clear" data-columns>';
					
					foreach ($news_array as $key => $item) {
						echo picto_echo_news( $item, 'agenda-cat' );
					}
					
					echo '</div><!-- #grid -->';
							
			} // if $news_array

		} // if $acf_agenda_cat


