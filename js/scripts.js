jQuery( document ).ready( function( $ ) {	


/* 
 * 0: js-hidden must be hidden
 ****************************************************
 */ 
 $(".js-hidden").hide();
 
 
/* 
 * Outgoing Links = new window
 ****************************************************
 */ 

$("a[href^=http]").each(
   function(){ 
      if(this.href.indexOf(location.hostname) == -1) {
  			$(this).attr('target', '_blank');
			}
  	}
 );
 
 
 
/*
 * Add numbers to Fotorama footer
 ********************************
 */ 
 
$(".fotorama__nav__shaft").each(
	function() {
		var count_elements = $(this).children().length;
		var count_elements = (count_elements - 1);
		
		if ( count_elements > 1) {
		
			// 1 - append number in html
			$(this).children(".fotorama__nav__frame--dot").each(function(i) {
				$(this).append( "<div class='foto-nr'>"+ (i+1) +"</div>" );
			});
			
			// 2 - append total number
			$(this).append( "<div class='final-nr fotorama__nav__frame fotorama__nav__frame--dot'><div class='foto-nr'>/"+ count_elements +"</div></div>" )
		
		} // end if > 1
	} // end function
); 

/*
 * Move fotorama caption under the block
 ****************************************************
 * Disabled: see https://bitbucket.org/ms-studio/picto/issues/8/
 */ 


/* 
 * that's it !
 ****************************************************
 */
 				
}); // end document ready